FROM golang:1.16-alpine

WORKDIR /app

# Download dependencies
COPY go.mod go.sum ./
RUN go mod download

# Build application
COPY . .
RUN go build -o /usr/bin/squirrel

# Run application
CMD [ "squirrel" ]