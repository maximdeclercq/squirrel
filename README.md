# Squirrel

A project to back up your Docker containers safely to the cloud.

Squirrel is free software. See the files whose names start with COPYING for
copying permission.

Copyright years on Squirrel source files may be listed using range notation,
e.g., 2020-2025, indicating that every year in the range, inclusive, is a
copyrightable year that could otherwise be listed individually.
