// Copyright: (c) 2020-2021, Maxim De Clercq
// GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

package main

import (
	"context"
	"fmt"
	"squirrel/pkg/config"
)

func main() {
	ctx := context.Background()
	conf := config.Parse(ctx)
	for key, value := range conf.Containers {
		fmt.Printf("%s: %+v\n", key, value)
	}
}
