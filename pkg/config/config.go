// Copyright: (c) 2020-2021, Maxim De Clercq
// GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

package config

import (
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"squirrel/pkg/definition"
)

type Config struct {
	Containers map[string]ContainerConfig
}

type ContainerConfig struct {
	Dump     string              // Name to Dump to
	Load     string              // Name to Load from
	Schedule string              // Cron Schedule
	Strategy definition.Strategy // Strategy to apply
	Provider definition.Provider // Provider to Dump files to or Load files from
}

func New() Config {
	return Config{Containers: make(map[string]ContainerConfig)}
}

func Parse(ctx context.Context) Config {
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	if err != nil {
		panic(err)
	}

	def, err := definition.Load()
	if err != nil {
		panic(err)
	}

	conf := New()
	for _, container := range containers {
		c, ok := ContainerConfig{}, true
		for key, value := range container.Labels {
			switch key {
			case "squirrel.dump":
				c.Dump = value
			case "squirrel.load":
				c.Load = value
			case "squirrel.schedule":
				c.Schedule = value
			case "squirrel.strategy":
				if c.Strategy, ok = def.GetStrategy(value); !ok {
					panic(fmt.Sprintf("Strategy '%s' does not exist!", value))
				}
			case "squirrel.provider":
				if c.Strategy, ok = def.GetStrategy(value); !ok {
					panic(fmt.Sprintf("Provider '%s' does not exist!", value))
				}
			}
		}
		conf.Containers[container.ID] = c
	}
	return conf
}
