// Copyright: (c) 2020-2021, Maxim De Clercq
// GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

package definition

import (
	_ "embed"
	"gopkg.in/yaml.v2"
)

//go:embed definition.yml
var definitionBytes []byte

type Definition struct {
	Strategies map[string]Strategy
	Providers  map[string]Provider
}

func New() Definition {
	return Definition{
		Strategies: make(map[string]Strategy),
		Providers:  make(map[string]Provider),
	}
}

func Load() (Definition, error) {
	var def Definition
	if err := yaml.Unmarshal(definitionBytes, &def); err != nil {
		return Definition{}, err
	}
	return def, nil
}

func (d Definition) Dump() ([]byte, error) {
	return yaml.Marshal(d)
}

// GetStrategy Shortcut for getting a Strategy
func (d Definition) GetStrategy(name string) (Strategy, bool) {
	s, ok := d.Strategies[name]
	return s, ok
}

// GetProvider Shortcut for getting a Provider
func (d Definition) GetProvider(name string) (Provider, bool) {
	p, ok := d.Providers[name]
	return p, ok
}
