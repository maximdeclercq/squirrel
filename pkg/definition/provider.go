// Copyright: (c) 2020-2021, Maxim De Clercq
// GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

package definition

// Provider Type of storage provider
type Provider int

const (
	Local Provider = iota // Local filesystem (default)
	GCS                   // Google Cloud Storage
	S3                    // Amazon Simple Storage Service
	Swift                 // Openstack Object Storage
	Rsync                 // Generic Rsync
)

// String Human readable String of the Provider
func (p Provider) String() string {
	return [...]string{"Local", "GCS", "S3", "Swift", "Rsync"}[p]
}

// EnumIndex Index of the Provider
func (p Provider) EnumIndex() int {
	return int(p)
}
