// Copyright: (c) 2020-2021, Maxim De Clercq
// GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/)

package definition

import (
	"bytes"
	"context"
	_ "embed"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
	"io/ioutil"
)

// Strategy How to backup a specific container
type Strategy struct {
	DumpCmd []string `yaml:"dump_cmd,omitempty"` // Cmd to execute in the container
	LoadCmd []string `yaml:"load_cmd,omitempty"` // Cmd to execute in the container
}

type ExecResult struct {
	ExitCode int
	StdOut   []byte
	StdErr   []byte
}

func (s Strategy) Dump(ctx context.Context, containerId string) (ExecResult, error) {
	var execResult ExecResult
	cli, err := client.NewClientWithOpts()
	if err != nil {
		return execResult, err
	}

	create, err := cli.ContainerExecCreate(ctx, containerId, types.ExecConfig{AttachStdout: true, AttachStderr: true, Cmd: s.DumpCmd})
	if err != nil {
		panic(err)
	}
	id := create.ID

	attach, err := cli.ContainerExecAttach(ctx, create.ID, types.ExecStartCheck{})
	if err != nil {
		panic(err)
	}

	// read the output
	var outBuf, errBuf bytes.Buffer
	outputDone := make(chan error)
	go func() {
		// StdCopy demultiplexes the stream into two buffers
		_, err = stdcopy.StdCopy(&outBuf, &errBuf, attach.Reader)
		outputDone <- err
	}()

	select {
	case err := <-outputDone:
		if err != nil {
			return execResult, err
		}
		break

	case <-ctx.Done():
		return execResult, ctx.Err()
	}

	stdout, err := ioutil.ReadAll(&outBuf)
	if err != nil {
		return execResult, err
	}
	stderr, err := ioutil.ReadAll(&errBuf)
	if err != nil {
		return execResult, err
	}

	res, err := cli.ContainerExecInspect(ctx, id)
	if err != nil {
		return execResult, err
	}

	execResult.ExitCode = res.ExitCode
	execResult.StdOut = stdout
	execResult.StdErr = stderr
	return execResult, nil
}
